package br.mobilit.calculadorafinanceira

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.card_item_list.view.*

class ConstantViewHolder  (itemView: View) : RecyclerView.ViewHolder(itemView){
    val nameConstant = itemView.tv_name
    val siteConstant = itemView.tv_site
    val imageConstant = itemView.iv_constant
    val valueConstant = itemView.tv_value
    val buttonShare = itemView.btn_share
    val buttonCopy = itemView.btn_copy
}
