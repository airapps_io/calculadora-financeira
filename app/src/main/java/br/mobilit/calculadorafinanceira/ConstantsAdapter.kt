package br.mobilit.calculadorafinanceira

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


class ConstantsAdapter() : RecyclerView.Adapter<ConstantViewHolder>() {

    //https://pt.wikipedia.org/wiki/Lista_de_constantes_matem%C3%A1ticas
    //cadastrar o resto das constantes
    val constant = listOf<Constant>(
            Constant("PI", 3.14159, "PI", "https://pt.wikipedia.org/wiki/Pi", R.drawable.ic_menu_share),
            Constant("SQRTDOIS", 1.41421, "Constante de Pitágoras (raíz quadrada de dois)", "https://pt.wikipedia.org/wiki/Raiz_quadrada_de_dois", R.drawable.ic_menu_share),
            Constant("NAPIER", 2.71828, "Constante de Napier (base do logaritmo natural)", "https://pt.wikipedia.org/wiki/N%C3%BAmero_de_Euler", R.drawable.ic_menu_share),
            Constant("EULER", 0.57721, "Constante de Euler-Mascheroni", "https://pt.wikipedia.org/wiki/Constante_de_Euler-Mascheroni", R.drawable.ic_menu_slideshow),
            Constant("OURO", 1.61803, "Número de Ouro, proporção áurea", "https://pt.wikipedia.org/wiki/Propor%C3%A7%C3%A3o_%C3%A1urea", R.drawable.ic_menu_share),
            Constant("EMBREEN", 0.70258, "Constante de Embree-Trefethen", "https://pt.wikipedia.org/w/index.php?title=Constante_de_Embree-Trefethen&action=edit&redlink=1", R.drawable.ic_menu_send))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConstantViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_item_list, parent, false)
        return ConstantViewHolder(view)
    }

    override fun getItemCount(): Int {
        return constant.size
    }

    override fun onBindViewHolder(holder: ConstantViewHolder, position: Int) {
        holder.imageConstant.setImageResource(constant[position].image)
        holder.valueConstant.text = constant[position].value.toString()
        holder.nameConstant.text = constant[position].desc

        holder.siteConstant.setOnClickListener {
            ContextCompat.startActivity(it.context, Intent(Intent.ACTION_VIEW, Uri.parse(constant[position].url)), null)
        }

        holder.buttonCopy.setOnClickListener {
            val sb = StringBuilder()
            sb.append("Você está usando o app Calculadora Matemática Financeira")
            sb.append("\n")
            sb.append("Baixe você também, é grátis")
            sb.append("\n")
            sb.append(constant[position].desc)
            sb.append(" = ")
            sb.append(constant[position].value)
            sb.append("\n")

            copyText(it, sb.toString())
        }
    }

    fun copyText(view: View, text: String) {
        val clipboard = view.context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
        val clip = ClipData.newPlainText("phrase", text)
        Snackbar.make(view, "Informações copiadas", Snackbar.LENGTH_LONG).show()
        clipboard!!.setPrimaryClip(clip)
    }
}