package br.mobilit.calculadorafinanceira

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class SimpleInterestActivity : AppCompatActivity() {

    private lateinit var et_maily: EditText
    private lateinit var et_interest_rate: EditText
    private lateinit var et_interest_period: EditText
    private lateinit var tv_result: TextView
    private lateinit var btn_calc: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_interest)

        et_maily = findViewById(R.id.et_maily)
        et_interest_rate = findViewById(R.id.et_interest_rate)
        et_interest_period = findViewById(R.id.et_interest_period)
        tv_result = findViewById(R.id.tv_result)
        btn_calc = findViewById(R.id.btn_calc)

        btn_calc.setOnClickListener {
            val valueMain = et_maily.text.toString().toDouble()
            val valueInterestRate = et_interest_rate.text.toString().toDouble()
            val valueInterestPeriod =  et_interest_period.text.toString().toDouble()

            val total = (valueMain * valueInterestRate * valueInterestPeriod) / 100

            tv_result.setText("R$" + total.toString())
        }
    }
}
