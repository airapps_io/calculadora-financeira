package br.mobilit.calculadorafinanceira

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button

class StringAdapter(private val mContext: Context) : BaseAdapter() {

    private val mThumbIds = arrayOf<String>("juros simples", "juros compostos",
            "soma", "subração",
            "multiplicação", "divisão",
            "potenciação", "radiciação",
            "módulos", "constantes",
            "regra de três", "taxa de juros",
            "Inequações", "funções exponenciais",
            "funções Logarítmicas", "operações trigonométricas",
            "derivadas", "integrais",
            "PA", "PG",
            "SEN - COS - TAN", "Equação do primeiro grau",
            "Equação do segundo grau")

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val button: Button

        if (convertView == null) {
            button = Button(mContext)
            button.layoutParams = ViewGroup.LayoutParams(300, 250)

        } else {
            button = convertView as Button
        }
        button.setText(mThumbIds[position])

        button.setOnClickListener {
            when (mThumbIds[position]) {
                "constantes" -> ContextCompat.startActivity(mContext, Intent(mContext, ConstantsActivity::class.java), null)
                "juros simples" -> ContextCompat.startActivity(mContext, Intent(mContext, SimpleInterestActivity::class.java), null)
            }
            //ContextCompat.startActivity(mContext, Intent(mContext, ConstantsActivity::class.java), null)
        }
        return button
    }

    override fun getItem(position: Int): Any {
        return mThumbIds.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0L
    }

    override fun getCount(): Int {
        return mThumbIds.size
    }

}
